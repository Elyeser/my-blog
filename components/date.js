import { isValid, parseISO, format } from "date-fns";
import frLocale from "date-fns/locale/fr-CA";

export default function Date({ dateString }) {
  if (!isValid(parseISO(dateString))) {
    return "No date";
  }
  const date = parseISO(dateString);
  return (
    <time className="capitalize" dateTime={dateString}>
      {format(date, "LLLL	d, yyyy", { locale: frLocale })}
    </time>
  );
}
